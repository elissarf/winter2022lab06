import java.util.Random;

public class Die{
	private int pips;
	private Random random;
	
	public Die(){
		pips= 1;
		random= new Random();
	}
	
	public int getPips(){
		return this.pips;
	}
	
	public void roll(){
		this.pips= random.nextInt(7);
	}
	
	public String toString(){
		return "" + pips;
	}
}