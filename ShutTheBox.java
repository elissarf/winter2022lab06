public class ShutTheBox {
	public static void main(String[] args) {
		System.out.println("WLECOME TO GAME");
		Board b = new Board();
		boolean gameOver = false;

		while (!gameOver) {
			System.out.println("Player 1's turn");
			System.out.println(b);

			gameOver = b.playATurn();
			if (gameOver) {
				System.out.println("Player 2 wins");
			} else {
				System.out.println("Player 2's turn");
				System.out.println(b);

				gameOver = b.playATurn();
				if (gameOver) {
					System.out.println("Player 1 wins");
				}
			}

		}
	}
}