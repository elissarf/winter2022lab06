public class Board {
	private Die roll1;
	private Die roll2;
	private boolean[] closedTiles;

	public Board() {
		roll1 = new Die();
		roll2 = new Die();
		closedTiles = new boolean[12];
	}

	public String toString() {
		String boardName = "";
		for (int i = 0; i < closedTiles.length; i++) {
			if (closedTiles[i]) {
				boardName = boardName + "X" + " ";
			} else {
				boardName = boardName + (i + 1) + " ";
			}
		}
		return boardName;
	}

	public boolean playATurn() {

		roll1.roll();
		roll2.roll();

		System.out.println("first roll is " + roll1 + " and second roll is " + roll2);

		int sum = roll1.getPips() + roll2.getPips();

		if (this.closedTiles[sum - 1]) {
			System.out.println("the tile is shut already");
			return true;
		} else {
			this.closedTiles[sum - 1] = true;
			System.out.println("Closing tile: " + sum);
			return false;
		}
	}
}
